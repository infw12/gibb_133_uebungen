<?php
// kurze Variablennamen erstellen
$DOCUMENT_ROOT = $HTTP_SERVER_VARS['DOCUMENT_ROOT'];

?>
<html>
<head>
  <title>Bob's Auto Parts - Kundenbestellungen</title>
</head>
<body>
<h1>Bob's Auto Parts</h1>
<h2>Kundenbestellungen</h2>
<?php
@$fp = fopen("orders.txt", 'r');
if (!$fp) {
    echo '<p><strong> Leider k�nnen wir Ihre Bestellung zur Zeit nicht bearbeiten.  '
       .' Bitte versuchen Sie es sp�ter noch einmal.</strong></p></body></html>';
    exit;
}
if (!$fp) {
    echo '<p><strong>Keine offenen Bestellungen.'
     . 'Versuchen Sie es sp�ter noch einmal.</strong></p></body></html>';
    exit;
} while (!feof($fp)) {
    $order = fgets($fp, 999);
    echo $order . '<br />';
}

echo 'Die Position des Dateizeigers zeigt auf '.(ftell($fp));
echo '<br />';
rewind($fp);
echo 'Nach rewind ist die Position '.(ftell($fp));
echo '<br />';

fclose($fp);

?>
</body>
</html>