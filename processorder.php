<?php
// kurze Variablennamen erstellen
$tireqty  = $_POST['tireqty'];
$oilqty   = $_POST['oilqty'];
$sparkqty = $_POST['sparkqty'];
$address  = $_POST['address'];

$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];

?>
<html>
<head>
  <title>Bob's Auto Parts - Ihre Bestellung</title>
</head>
<body>
<h1>Bob's Auto Parts</h1>
<h2>Ihre Bestellung</h2>
<?php
$date = date('H:i, jS F');

echo '<p>Bestellung ausgef�hrt am ';
echo $date;
echo '</p>';

echo '<p>Ihre Bestellung: </p>';

$totalqty = 0;
$totalqty = $tireqty + $oilqty + $sparkqty;
echo 'Bestellte Artikel: ' . $totalqty . '<br />';

if ($totalqty == 0) {
    echo 'Sie haben auf der vorhergehenden Seite nichts bestellt!<br />';
} else {
    if ($tireqty > 0) {
        echo $tireqty . ' Reifen<br />';
    }
    if ($oilqty > 0) {
        echo $oilqty . ' �l<br />';
    }
    if ($sparkqty > 0) {
        echo $sparkqty . ' Z�ndkerzen<br />';
    }
}
$totalamount = 0.00;

define('TIREPRICE', 100);
define('OILPRICE', 10);
define('SPARKPRICE', 4);

$totalamount = $tireqty * TIREPRICE + $oilqty * OILPRICE + $sparkqty * SPARKPRICE;

$totalamount = number_format($totalamount, 2, '.', ' ');

echo '<p>Gesamtsumme ' . $totalamount . '</p>';
echo '<p>Versandadresse ' . $address . '</p>';

$outputstring = $date . "\t" . $tireqty . " Reifen\t" . $oilqty . " �l\t"
              . $sparkqty . " Z�ndkerzen\t�"
              . $totalamount . "\t" . $address . "\n";

// Datei zum Anh�ngen �ffnen
//@ $fp = fopen("$DOCUMENT_ROOT/../orders/orders.txt", 'ab');
$fp = fopen("orders.txt", 'ab');

flock($fp, LOCK_EX);

if (!$fp) {
    echo '<p><strong> Leider k�nnen wir Ihre Bestellung zur Zeit nicht bearbeiten.  '
     . 'Bitte versuchen Sie es sp�ter noch einmal.</strong></p></body></html>';
    exit;
}

fwrite($fp, $outputstring, strlen($outputstring));
flock($fp, LOCK_UN);
fclose($fp);

echo '<p>Bestellung aufgenommen.</p>';
?>
</body>
</html>